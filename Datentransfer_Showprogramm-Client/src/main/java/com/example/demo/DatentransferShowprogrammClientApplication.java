package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@EnableAutoConfiguration
@EnableEurekaClient
@RestController
@EnableFeignClients
public class DatentransferShowprogrammClientApplication {
	
	@Autowired
	private FeignInterface client;
	
	public static void main(String[] args) {
	SpringApplication.run(DatentransferShowprogrammClientApplication.class, args);

		

	
		}
	   @RequestMapping("/greeting")
	    public String greeting() {
	        System.out.println(client.ressponse());
	        return client.ressponse();
	    }

	
}
