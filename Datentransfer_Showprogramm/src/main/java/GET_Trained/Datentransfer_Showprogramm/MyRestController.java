package GET_Trained.Datentransfer_Showprogramm;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class MyRestController {
//Eureka server?
	
    @RequestMapping("/{id}")
    public String showProduct(@PathVariable String id){
        return "Hello World: "+id;
    }
}